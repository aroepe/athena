#include "../SimKernel.h"
#include "../SimKernelMT.h"
#include "../CollectionMerger.h"
#include "../SimHitTreeCreator.h"
#include "../SimEventFilter.h"
#include "../RenameHitCollectionsAlg.h"

DECLARE_COMPONENT( ISF::SimKernel )
DECLARE_COMPONENT( ISF::SimKernelMT )
DECLARE_COMPONENT( ISF::CollectionMerger )
DECLARE_COMPONENT( ISF::SimHitTreeCreator )
DECLARE_COMPONENT( ISF::SimEventFilter )
DECLARE_COMPONENT( ISF::RenameHitCollectionsAlg )
