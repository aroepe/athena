# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#====================================================================
# DAOD_PHYSLLP.py
# This defines DAOD_PHYSLLP, an unskimmed DAOD format for Run 3.
# It contains the variables and objects needed for the large majority 
# of physics analyses in ATLAS.
# It requires the reductionConf flag PHYSLLP in Reco_tf.py   
#====================================================================

from AthenaCommon import Logging
nanolog = Logging.logging.getLogger('PHYSLLP')

from DerivationFrameworkCore.DerivationFrameworkMaster import buildFileName
from DerivationFrameworkCore.DerivationFrameworkMaster import DerivationFrameworkIsMonteCarlo, DerivationFrameworkJob
from DerivationFrameworkPhys import PhysCommon
from DerivationFrameworkEGamma import ElectronsCPDetailedContent
from DerivationFrameworkJetEtMiss import METCommon
from DerivationFrameworkJetEtMiss.METCommon import scheduleMETAssocAlg
from DerivationFrameworkCore import LHE3WeightMetadata

#====================================================================
# Set up sequence for this format and add to the top sequence 
#====================================================================
SeqPHYSLLP = CfgMgr.AthSequencer("SeqPHYSLLP")
DerivationFrameworkJob += SeqPHYSLLP

#====================================================================
# SET UP STREAM   
#====================================================================
streamName = derivationFlags.WriteDAOD_PHYSLLPStream.StreamName
fileName   = buildFileName( derivationFlags.WriteDAOD_PHYSLLPStream )
PHYSLLPStream = MSMgr.NewPoolRootStream( streamName, fileName )
PHYSLLPStream.AcceptAlgs(["PHYSLLPKernel"])

### Thinning and augmentation tools lists
thinningTools       = []
AugmentationTools   = []

# Special sequence 
SeqPHYSLLP = CfgMgr.AthSequencer("SeqPHYSLLP")


#=============================================================================================    
# SKIMMING - skimming on triggers listed in python/PHYSLLPTriggerList.py                
#=============================================================================================        

from DerivationFrameworkPhys.PHYSLLPTriggerList import triggers_lep, triggers_photon

triggers = triggers_lep + triggers_photon

expression_trigger = "(" + " || ".join(triggers) + ")"

expression = expression_trigger

print ("expression = '" + expression + "'")
print ()

from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__xAODStringSkimmingTool
PHYSLLPSkimmingTool = DerivationFramework__xAODStringSkimmingTool( name = "PHYSLLPSkimmingTool", expression = expression)

ToolSvc += PHYSLLPSkimmingTool

SeqPHYSLLP += CfgMgr.DerivationFramework__DerivationKernel(
        "PHYSLLPKernelSkim",
        SkimmingTools = [PHYSLLPSkimmingTool],
)



#--------------------------------------------------------------
# Run the LRT merger
#--------------------------------------------------------------
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackParticleMerger
LRTAndStandardTrackParticleMerger = DerivationFramework__TrackParticleMerger(name                        = "LRTAndStandardTrackParticleMerger",
                                                                             TrackParticleLocation       = ["InDetTrackParticles","InDetLargeD0TrackParticles"],
                                                                             OutputTrackParticleLocation = "InDetWithLRTTrackParticles",
                                                                             CreateViewColllection       = True)

ToolSvc += LRTAndStandardTrackParticleMerger
SeqPHYSLLP += CfgMgr.DerivationFramework__CommonAugmentation("InDetWithLRTLRTMerge",
                                                                         AugmentationTools = [LRTAndStandardTrackParticleMerger])


#--------------------------------------------------------------
# Run VSI
#--------------------------------------------------------------
#include ("InDetRecExample/PixelConditionsAccess.py") # include all pixel condtions avaliable in AOD /DT



from VrtSecInclusive.VrtSecInclusive import VrtSecInclusive
from VrtSecInclusive.VrtSecInclusive_Configuration import setupVSI
from TrkExTools.AtlasExtrapolator import AtlasExtrapolator

ToolSvc += AtlasExtrapolator()

# set options related to the vertex fitter
from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
InclusiveVxFitterTool = Trk__TrkVKalVrtFitter(name                = "InclusiveVxFitter",
                                              Extrapolator        = ToolSvc.AtlasExtrapolator,
                                              IterationNumber     = 30,
                                              )
ToolSvc +=  InclusiveVxFitterTool;
InclusiveVxFitterTool.OutputLevel = INFO


VrtSecInclusive_InDet = setupVSI( "InDet" )

VrtSecInclusive_InDet.VertexFitterTool        = InclusiveVxFitterTool
VrtSecInclusive_InDet.Extrapolator            = ToolSvc.AtlasExtrapolator
VrtSecInclusive_InDet.FillIntermediateVertices= False
#VrtSecInclusive_InDet.TrackLocation           = "InDetTrackParticles"

SeqPHYSLLP += VrtSecInclusive_InDet

"""
doWriteAOD = False
doWriteESD = False

OutputLevel     = ERROR 
doJiveXML       = False
doVP1           = False
doAuditors      = False
doEdmMonitor    = False
doNameAuditor   = False

# --- setup InDetJobProperties
from InDetRecExample.InDetJobProperties import InDetFlags
InDetFlags.doPrintConfigurables = True

from AthenaCommon.DetFlags import DetFlags 
# --- switch on InnerDetector
DetFlags.ID_setOn()
# --- and switch off all the rest
DetFlags.Calo_setOff()
DetFlags.Muon_setOff()
# --- printout
DetFlags.Print()

include("InDetRecExample/InDetRec_all.py")
"""


PHYSLLPStream.AddItem( [ 'xAOD::TrackParticleContainer#InDet*TrackParticles*',
                       'xAOD::TrackParticleAuxContainer#InDet*TrackParticles*',
                       'xAOD::VertexContainer#VrtSecInclusive*',
                       'xAOD::VertexAuxContainer#VrtSecInclusive*'] )

print("List of items for the DAOD_RPVLL output stream:")
print(PHYSLLPStream.GetItems())

#====================================================================
# THINNING
#====================================================================
# ID tracks: See recommedations here: 
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/DaodRecommendations

'''
# Inner detector group recommendations for indet tracks in analysis
PHYSLLP_thinning_expression = "(InDetTrackParticles.pt > 1*GeV) && ((InDetTrackParticles.numberOfPixelHits + InDetTrackParticles.numberOfSCTHits) > 1) && (InDetTrackParticles.chiSquared/InDetTrackParticles.numberDoF < 5) && (InDetTrackParticles.is_selected_InDet)"
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackParticleThinning
PHYSLLPTrackParticleThinningTool = DerivationFramework__TrackParticleThinning(name                 = "PHYSLLPTrackParticleThinningTool",
                                                                           StreamName              = PHYSLLPStream.Name, 
                                                                           SelectionString         = PHYSLLP_thinning_expression,
                                                                           InDetTrackParticlesKey  = "InDetTrackParticles")

ToolSvc += PHYSLLPTrackParticleThinningTool
thinningTools.append(PHYSLLPTrackParticleThinningTool)



PHYSLLP_ld_thinning_expression = "(InDetLargeD0TrackParticles.pt > 1*GeV) && ((InDetLargeD0TrackParticles.numberOfPixelHits + InDetLargeD0TrackParticles.numberOfSCTHits) > 1)"
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackParticleThinning
PHYSLLPLDTrackParticleThinningTool = DerivationFramework__TrackParticleThinning(name               = "PHYSLLPLDTrackParticleThinningTool",
                                                                           StreamName              = PHYSLLPStream.Name,
                                                                           SelectionString         = PHYSLLP_ld_thinning_expression,
                                                                           InDetTrackParticlesKey  = "InDetLargeD0TrackParticles")

ToolSvc += PHYSLLPLDTrackParticleThinningTool
thinningTools.append(PHYSLLPLDTrackParticleThinningTool)

# Include inner detector tracks associated with muons
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__MuonTrackParticleThinning
PHYSLLPMuonTPThinningTool = DerivationFramework__MuonTrackParticleThinning(name                 = "PHYSLLPMuonTPThinningTool",
                                                                        StreamName              = PHYSLLPStream.Name,
                                                                        MuonKey                 = "Muons",
                                                                        SelectionString         = "Muons.pt > 10*GeV",
                                                                        InDetTrackParticlesKey  = "InDetTrackParticles")

ToolSvc += PHYSLLPMuonTPThinningTool
thinningTools.append(PHYSLLPMuonTPThinningTool)

# TrackParticles associated with electrons
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__EgammaTrackParticleThinning
PHYSLLPElectronTPThinningTool = DerivationFramework__EgammaTrackParticleThinning(name                   = "PHYSLLPElectronTPThinningTool",
                                                                                 StreamName             = PHYSLLPStream.Name,
                                                                                 SGKey                  = "Electrons",
                                                                                 BestMatchOnly          = False,
                                                                                 SelectionString        = "Electrons.pt > 10*GeV && Electrons.Loose",
                                                                                 InDetTrackParticlesKey = "InDetTrackParticles")
ToolSvc += PHYSLLPElectronTPThinningTool
thinningTools.append(PHYSLLPElectronTPThinningTool)

# TrackParticles associated with taus
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TauTrackParticleThinning
PHYSLLPTauTPThinningTool = DerivationFramework__TauTrackParticleThinning( name                 = "PHYSLLPTauTPThinningTool",
                                                                        StreamName             = PHYSLLPStream.Name,
                                                                        TauKey                 = "TauJets",
                                                                        InDetTrackParticlesKey = "InDetTrackParticles")
ToolSvc += PHYSLLPTauTPThinningTool
thinningTools.append(PHYSLLPTauTPThinningTool)



#Tracks associated with Jets
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__JetTrackParticleThinning
PHYSLLPAKt4JetTPThinningTool = DerivationFramework__JetTrackParticleThinning( name                  = "PHYSLLPAKt4JetTPThinningTool",
                                                                            StreamName              = PHYSLLPStream.Name,
                                                                            JetKey                  = "AntiKt4EMTopoJets",
                                                                            SelectionString         = "AntiKt4EMTopoJets.pt > 20*GeV && abs(AntiKt4EMTopoJets.eta) < 2.1",
                                                                            InDetTrackParticlesKey  = "InDetTrackParticles")
ToolSvc += PHYSLLPAKt4JetTPThinningTool
thinningTools.append(PHYSLLPAKt4JetTPThinningTool)
'''

print ("standard track DV thinning")
#Tracks associated with DVs
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__VSITrackParticleThinning
PHYSLLPVSITPThinningTool = DerivationFramework__VSITrackParticleThinning( name                  = "PHYSLLPVSITPThinningTool",
                                                                            StreamName              = PHYSLLPStream.Name,
                                                                            InDetTrackParticlesKey  = "InDetTrackParticles")
                                                                          #TrackSelectionString          = "InDetTrackParticles.is_svtrk_final_InDet == 1")
                    
ToolSvc += PHYSLLPVSITPThinningTool
thinningTools.append(PHYSLLPVSITPThinningTool)

from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__VSITrackParticleThinning
PHYSLLPLDVSITPThinningTool = DerivationFramework__VSITrackParticleThinning( name                  = "PHYSLLPLDVSITPThinningTool",
                                                                            StreamName              = PHYSLLPStream.Name,
                                                                            InDetTrackParticlesKey  = "InDetLargeD0TrackParticles")
                                                                            #TrackSelectionString        = "InDetLargeD0TrackParticles.is_svtrk_final_InDet == 1")
ToolSvc += PHYSLLPLDVSITPThinningTool
thinningTools.append(PHYSLLPLDVSITPThinningTool)

'''
# Muon thinning
muon_thinning_expression = "(Muons.pt > 10*GeV)"
from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__GenericObjectThinning
PHYSLLPMuonThinningTool = DerivationFramework__GenericObjectThinning(name            = "PHYSLLPMuonThinningTool",
                                                                     StreamName      = PHYSLLPStream.Name,
                                                                     ContainerName   = "Muons",
                                                                     SelectionString = muon_thinning_expression)
ToolSvc += PHYSLLPMuonThinningTool
thinningTools.append(PHYSLLPMuonThinningTool)

# Electron thinning
electron_thinning_expression = "(Electrons.pt > 10*GeV) && (Electrons.Loose)"
from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__GenericObjectThinning
PHYSLLPElectronThinningTool = DerivationFramework__GenericObjectThinning(name        = "PHYSLLPElectronThinningTool",
                                                                     StreamName      = PHYSLLPStream.Name,
                                                                     ContainerName   = "Electrons",
                                                                     SelectionString = electron_thinning_expression)
ToolSvc += PHYSLLPElectronThinningTool
thinningTools.append(PHYSLLPElectronThinningTool)

# Photon thinning
photon_thinning_expression = "(Photons.pt > 60*GeV) && (Photons.Loose)"
from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__GenericObjectThinning
PHYSLLPPhotonThinningTool = DerivationFramework__GenericObjectThinning(name          = "PHYSLLPPhotonThinningTool",
                                                                     StreamName      = PHYSLLPStream.Name,
                                                                     ContainerName   = "Photons",
                                                                     SelectionString = photon_thinning_expression)
ToolSvc += PHYSLLPPhotonThinningTool
thinningTools.append(PHYSLLPPhotonThinningTool)

# Jet thinning
jet_thinning_expression = "(AntiKt4EMTopoJets.pt > 15*GeV) && (abs(AntiKt4EMTopoJets.eta) < 2.1)"
from DerivationFrameworkTools.DerivationFrameworkToolsConf import DerivationFramework__GenericObjectThinning
PHYSLLPJetThinningTool = DerivationFramework__GenericObjectThinning(name            = "PHYSLLPJetThinningTool",
                                                                     StreamName      = PHYSLLPStream.Name,
                                                                     ContainerName   = "AntiKt4EMTopoJets",
                                                                     SelectionString = jet_thinning_expression)
ToolSvc += PHYSLLPJetThinningTool
thinningTools.append(PHYSLLPJetThinningTool)
'''
#====================================================================
# CREATE THE DERIVATION KERNEL ALGORITHM   
#====================================================================
# Add the kernel for thinning (requires the objects be defined)
#from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__DerivationKernel
SeqPHYSLLP += CfgMgr.DerivationFramework__DerivationKernel("PHYSLLPKernel",
                                                        ThinningTools = thinningTools)

#====================================================================
# CONTENTS   
#====================================================================
from DerivationFrameworkCore.SlimmingHelper import SlimmingHelper
PHYSLLPSlimmingHelper = SlimmingHelper("PHYSLLPSlimmingHelper")

PHYSLLPSlimmingHelper.SmartCollections = ["Electrons",
                                       "Photons",
                                       "Muons",
                                       "PrimaryVertices",
                                       "AntiKt4EMTopoJets",
                                       #"AntiKt4EMPFlowJets",
                                       #"BTagging_AntiKt4EMPFlowJets",
                                       #"BTagging_AntiKtVR30Rmax4Rmin02Track", 
                                       "MET_Baseline_AntiKt4EMTopo",
                                       #"MET_Baseline_AntiKt4EMPFlow",
                                       #"AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                                       #"AntiKtVR30Rmax4Rmin02PV0TrackJets",
                                       #"BTagging_AntiKtVR30Rmax4Rmin02Track_201903"
                                      ]

excludedVertexAuxData = "-vxTrackAtVertex.-MvfFitInfo.-isInitialized.-VTAV"
StaticContent = []
#StaticContent += ["xAOD::VertexContainer#SoftBVrtClusterTool_Tight_Vertices"]
#StaticContent += ["xAOD::VertexAuxContainer#SoftBVrtClusterTool_Tight_VerticesAux." + excludedVertexAuxData]
#StaticContent += ["xAOD::VertexContainer#SoftBVrtClusterTool_Medium_Vertices"]
#StaticContent += ["xAOD::VertexAuxContainer#SoftBVrtClusterTool_Medium_VerticesAux." + excludedVertexAuxData]
#StaticContent += ["xAOD::VertexContainer#SoftBVrtClusterTool_Loose_Vertices"]
#StaticContent += ["xAOD::VertexAuxContainer#SoftBVrtClusterTool_Loose_VerticesAux." + excludedVertexAuxData]

PHYSLLPSlimmingHelper.StaticContent = StaticContent

# Trigger content
PHYSLLPSlimmingHelper.IncludeTriggerNavigation = False
PHYSLLPSlimmingHelper.IncludeJetTriggerContent = False
PHYSLLPSlimmingHelper.IncludeMuonTriggerContent = False
PHYSLLPSlimmingHelper.IncludeEGammaTriggerContent = False
PHYSLLPSlimmingHelper.IncludeJetTauEtMissTriggerContent = False
PHYSLLPSlimmingHelper.IncludeTauTriggerContent = False
PHYSLLPSlimmingHelper.IncludeEtMissTriggerContent = False
PHYSLLPSlimmingHelper.IncludeBJetTriggerContent = False
PHYSLLPSlimmingHelper.IncludeBPhysTriggerContent = False
PHYSLLPSlimmingHelper.IncludeMinBiasTriggerContent = False

# Truth containers
if DerivationFrameworkIsMonteCarlo:
   PHYSLLPSlimmingHelper.AppendToDictionary = {'TruthEvents':'xAOD::TruthEventContainer','TruthEventsAux':'xAOD::TruthEventAuxContainer',
                                            'MET_Truth':'xAOD::MissingETContainer','MET_TruthAux':'xAOD::MissingETAuxContainer',
                                            'TruthElectrons':'xAOD::TruthParticleContainer','TruthElectronsAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthMuons':'xAOD::TruthParticleContainer','TruthMuonsAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthPhotons':'xAOD::TruthParticleContainer','TruthPhotonsAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthTaus':'xAOD::TruthParticleContainer','TruthTausAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthNeutrinos':'xAOD::TruthParticleContainer','TruthNeutrinosAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthBSM':'xAOD::TruthParticleContainer','TruthBSMAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthBoson':'xAOD::TruthParticleContainer','TruthBosonAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthTop':'xAOD::TruthParticleContainer','TruthTopAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthForwardProtons':'xAOD::TruthParticleContainer','TruthForwardProtonsAux':'xAOD::TruthParticleAuxContainer',
                                            'BornLeptons':'xAOD::TruthParticleContainer','BornLeptonsAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthBosonsWithDecayParticles':'xAOD::TruthParticleContainer','TruthBosonsWithDecayParticlesAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthBosonsWithDecayVertices':'xAOD::TruthVertexContainer','TruthBosonsWithDecayVerticesAux':'xAOD::TruthVertexAuxContainer',
                                            'TruthBSMWithDecayParticles':'xAOD::TruthParticleContainer','TruthBSMWithDecayParticlesAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthBSMWithDecayVertices':'xAOD::TruthVertexContainer','TruthBSMWithDecayVerticesAux':'xAOD::TruthVertexAuxContainer',
                                            'HardScatterParticles':'xAOD::TruthParticleContainer','HardScatterParticlesAux':'xAOD::TruthParticleAuxContainer',
                                            'HardScatterVertices':'xAOD::TruthVertexContainer','HardScatterVerticesAux':'xAOD::TruthVertexAuxContainer',
                                            'TruthHFWithDecayParticles':'xAOD::TruthParticleContainer','TruthHFWithDecayParticlesAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthHFWithDecayVertices':'xAOD::TruthVertexContainer','TruthHFWithDecayVerticesAux':'xAOD::TruthVertexAuxContainer',
                                            'TruthCharm':'xAOD::TruthParticleContainer','TruthCharmAux':'xAOD::TruthParticleAuxContainer',
                                            'TruthPrimaryVertices':'xAOD::TruthVertexContainer','TruthPrimaryVerticesAux':'xAOD::TruthVertexAuxContainer',
                                            'AntiKt10TruthTrimmedPtFrac5SmallR20Jets':'xAOD::JetContainer', 'AntiKt10TruthTrimmedPtFrac5SmallR20JetsAux':'xAOD::JetAuxContainer'
                                           }

   from DerivationFrameworkMCTruth.MCTruthCommon import addTruth3ContentToSlimmerTool
   addTruth3ContentToSlimmerTool(PHYSLLPSlimmingHelper)
   PHYSLLPSlimmingHelper.AllVariables += ['TruthHFWithDecayParticles','TruthHFWithDecayVertices','TruthCharm']

PHYSLLPSlimmingHelper.AllVariables += ['EventInfo']
PHYSLLPSlimmingHelper.ExtraVariables += [#"AntiKt10TruthTrimmedPtFrac5SmallR20Jets.Tau1_wta.Tau2_wta.Tau3_wta.D2.GhostBHadronsFinalCount",
                                      "Electrons.TruthLink",
                                      "Muons.TruthLink",
                                      "Photons.TruthLink",
                                      #"AntiKt2PV0TrackJets.pt.eta.phi.m",
                                      "AntiKt4EMTopoJets.DFCommonJets_QGTagger_truthjet_nCharged.DFCommonJets_QGTagger_truthjet_pt.DFCommonJets_QGTagger_truthjet_eta.DFCommonJets_QGTagger_NTracks.DFCommonJets_QGTagger_TracksWidth.DFCommonJets_QGTagger_TracksC1.PartonTruthLabelID.ConeExclBHadronsFinal.ConeExclCHadronsFinal.GhostBHadronsFinal.GhostCHadronsFinal.GhostBHadronsFinalCount.GhostBHadronsFinalPt.GhostCHadronsFinalCount.GhostCHadronsFinalPt.GhostBHadronsFinal.GhostCHadronsFinal",
                                      #"AntiKt4EMPFlowJets.DFCommonJets_QGTagger_truthjet_nCharged.DFCommonJets_QGTagger_truthjet_pt.DFCommonJets_QGTagger_truthjet_eta.DFCommonJets_QGTagger_NTracks.DFCommonJets_QGTagger_TracksWidth.DFCommonJets_QGTagger_TracksC1.PartonTruthLabelID.DFCommonJets_fJvt.ConeExclBHadronsFinal.ConeExclCHadronsFinal.GhostBHadronsFinal.GhostCHadronsFinal.GhostBHadronsFinalCount.GhostBHadronsFinalPt.GhostCHadronsFinalCount.GhostCHadronsFinalPt.GhostBHadronsFinal.GhostCHadronsFinal",
                                     #"AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903.GhostBHadronsFinal.GhostCHadronsFinal.GhostBHadronsFinalCount.GhostBHadronsFinalPt.GhostCHadronsFinalCount.GhostCHadronsFinalPt.GhostTausFinal.GhostTausFinalCount",
                                     #"AntiKtVR30Rmax4Rmin02TrackJets_BTagging201810.GhostBHadronsFinal.GhostCHadronsFinal.GhostBHadronsFinalCount.GhostBHadronsFinalPt.GhostCHadronsFinalCount.GhostCHadronsFinalPt.GhostTausFinal.GhostTausFinalCount",
                                      "TruthPrimaryVertices.t.x.y.z",
                                      "PrimaryVertices.t.x.y.z",
                                      "InDetWithLRTTrackParticles.TTVA_AMVFVertices.TTVA_AMVFWeights"]

# Final construction of output stream
PHYSLLPSlimmingHelper.AppendContentToStream(PHYSLLPStream)

