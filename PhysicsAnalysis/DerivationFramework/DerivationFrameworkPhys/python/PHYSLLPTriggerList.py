# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration


from TriggerMenuMT.TriggerAPI.TriggerAPI import TriggerAPI 
from TriggerMenuMT.TriggerAPI.TriggerEnums import TriggerPeriod, TriggerType


periods= TriggerPeriod.y2015 | TriggerPeriod.y2016 | TriggerPeriod.y2017 | TriggerPeriod.y2018

triggers_lep = []
triggers_lep += TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2015, TriggerType.mu_single) + TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2015, TriggerType.el_single)
triggers_lep += TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2016, TriggerType.mu_single) + TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2016, TriggerType.el_single)
triggers_lep += TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2017, TriggerType.mu_single) + TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2017, TriggerType.el_single)
triggers_lep += TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2018, TriggerType.mu_single) + TriggerAPI.getLowestUnprescaled(TriggerPeriod.y2018, TriggerType.el_single)


triggers_photon = TriggerAPI.getLowestUnprescaled(periods, TriggerType.g)


'''
triggers_lep = [
                "HLT_mu60_0eta105_msonly",
                "HLT_mu80_msonly_3layersEC",
                "HLT_mu26_ivarmedium"
]

triggers_photon = [
                "HLT_g140_loose",
                "HLT_2g50_loose",
                "HLT_g200_loose",
                "HLT_g200_loose_L1EM24VHIM",
                "HLT_2g50_loose_L12EM20VH",
                "HLT_2g60_loose_L12EM20VH",
]
'''
